/*
 * CSc103 Project 3: unix utilities
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours:
 */

#include <iostream>
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <set>
using std::set;
#include <vector>
using std::vector;
#include <getopt.h> // to parse long arguments.
#include <cstdio> // printf

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of wc.  Supported options:\n\n"
"   -c,--bytes            print byte count.\n"
"   -l,--lines            print line count.\n"
"   -w,--words            print word count.\n"
"   -L,--max-line-length  print length of longest line.\n"
"   -u,--uwords           print unique word count.\n"
"   --help          show this message and exit.\n";

// functions
vector<char> getChar();
int countChar();
int countWord();
int countLine();
int countUWord();
int countLineLen();

// vector
vector<char> input;

int main(int argc, char *argv[])
{
	// define long options
	static int charonly=0, linesonly=0, wordsonly=0, uwordsonly=0, longonly=0;
	static struct option long_opts[] = {
		{"bytes",           no_argument,   0, 'c'},
		{"lines",           no_argument,   0, 'l'},
		{"words",           no_argument,   0, 'w'},
		{"uwords",          no_argument,   0, 'u'},
		{"max-line-length", no_argument,   0, 'L'},
		{"help",            no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "clwuLh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				charonly = 1;
				break;
			case 'l':
				linesonly = 1;
				break;
			case 'w':
				wordsonly = 1;
				break;
			case 'u':
				uwordsonly = 1;
				break;
			case 'L':
				longonly = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	getChar();
	int CountChar = countChar();
	int CountWord = countWord();
	int CountLine = countLine();
	int CountUWord = countUWord();
	int CountMaxLine = countLineLen();

	//displays
	if(charonly == 1)
		cout << CountChar << endl;
	if(wordsonly == 1)
		cout << CountWord << endl;
	if(linesonly == 1)
		cout << CountLine << endl;
	if(uwordsonly == 1)
		cout << CountUWord << endl;
	if(longonly == 1)
		cout << CountMaxLine << endl;

	if(charonly==0&&linesonly==0&&wordsonly==0&&uwordsonly==0&&longonly==0)
		cout << '\t' << CountLine << '\t' << CountWord << '\t' << CountLine << endl;

	return 0;
}

vector<char> getChar()
{
	char c;
	while(fread(&c,1,1,stdin))
	{
		input.push_back(c);
	}
	return input;
}

int countChar()
{
	return input.size();
}

int countWord()
{	
	int wordcount = 0;
	char pre;
	char cur;
	if(input[0] == ' ' || input[0] == '\t')
	{
		for(size_t i = 1; i < input.size(); i++)
		{
			cur = input[i];
			pre = input[i-1];
			if((cur==' '||cur=='\t'||cur=='\n') && (pre!=' '&&pre!='\t'&&pre!='\n'))
			{
				cout << "";
				wordcount++;
			}
		}
	}
	else
	{
		for(size_t i = 1; i < input.size(); i++)
		{
			cur = input[i];
			pre = input[i-1];
			if((cur==' '||cur=='\t'||cur=='\n') && (pre!=' '&&pre!='\t'&&pre!='\n'))
			{
				cout << "";
				wordcount++;
			}
		}
	}
	if((input[input.size()-1]!=' ')&&(input[input.size()-1]!='\t')&&(input[input.size()-1]!='\n'))
	{
		wordcount++;
	}
	return wordcount;
}

int countLine()
{
	int countline = 0;
	for(size_t i = 0; i < input.size(); i++)
	{
		if(input[i] == '\n')
		{
			countline++;
		}
	}
	return countline;
}

int countUWord()
{
	set<vector<char> > words;
	vector<vector<char> > bigvector;
	string inp;
	vector<char> stuff;
	vector<char> tmp;	
	char cur;
	char pre;
	for(size_t i = 0; i < input.size()-1; i++)
	{
		tmp.push_back(input[i]);
		cur = input[i];
		pre = input[i-1];
		if((cur==' '||cur=='\t'||cur=='\n') && (pre!=' '&&pre!='\t'&&pre!='\n'))
		{
			cout << "";
			bigvector.push_back(tmp);
			tmp.clear();
		}
	}
	for(size_t i = 0; i < bigvector.size(); i++)
	{
		vector<char> word = bigvector[i]; 
		words.insert(word);
	}
	return words.size();
}

int countLineLen()
{
	vector<vector<char> > bigvector;
	string inp;
	vector<char> stuff;
	int length = 0;
	vector<char> tmp;	
	for(size_t k = 0; k < input.size()-1; k++)
	{
		tmp.push_back(input[k]);
		if(input[k] == '\n')
		{
			bigvector.push_back(tmp);
			tmp.clear();
		}
	}
	for(size_t i = 0; i < bigvector.size(); i++)
	{
		int length = 0;
		vector<char> currentline = bigvector[i];
		for(size_t j = 0; j < currentline.size(); j++)
		{
			if(currentline[j] == '\t')
			{
				length += 8 - length % 8;
			}
			else
			{
				length++;
			}
		}
	}
	return length-1;
}