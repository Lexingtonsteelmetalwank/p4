/*
 * CSc103 Project 3: unix utilities
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: 
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <algorithm>
using std::sort;
#include <string.h> // for c-string functions.
#include <getopt.h> // to parse long arguments.

static const char* usage =
"Usage: %s [OPTIONS] SET1 [SET2]\n"
"Limited clone of tr.  Supported options:\n\n"
"   -c,--complement     Use the complement of SET1.\n"
"   -d,--delete         Delete characters in SET1 rather than translate.\n"
"   --help          show this message and exit.\n";

void escape(string& s) {
	/* NOTE: the normal tr command seems to handle invalid escape
	 * sequences by simply removing the backslash (silently) and
	 * continuing with the translation as if it never appeared. */
	/* TODO: write me... */
}

int main(int argc, char *argv[])
{
	// define long options
	static int comp=0, del=0;
	static struct option long_opts[] = {
		{"complement",      no_argument,   0, 'c'},
		{"delete",          no_argument,   0, 'd'},
		{"help",            no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "cdh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				comp = 1;
				break;
			case 'd':
				del = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
	if (del) {
		if (optind != argc-1) {
			fprintf(stderr, "wrong number of arguments.\n");
			return 1;
		}
	} else if (optind != argc-2) {
		fprintf(stderr,
				"Exactly two strings must be given when translating.\n");
		return 1;
	}
	string s1 = argv[optind++];
	string s2 = (optind < argc)?argv[optind]:"";
	/* process any escape characters: */
	escape(s1);
	escape(s2);

	/* TODO: finish this... */
	char letter;
	vector<char> input;

	while(fread(&letter,1,1,stdin))
	{
		input.push_back(letter);
	}

	if(comp == 1)
	{
		if(del == 1)
		{
			// DELETE (-d)
			for(vector<char>::iterator i = input.begin(); i != input.end(); i++)
			{
				for(string::iterator j = s1.begin(); j != s1.end(); j++)
				{
					if(*i == *j)
					{
						input.erase(i);
					}
				}
			}

			// COMPLIMENT (-c)
			for(vector<char>::iterator i = input.begin(); i != input.end(); i++)
			{
				for(string::iterator j = s1.begin(); j != s1.end(); j++)
				{
					if(*i != *j && *i != *(j+1))
						*i = *j;
					else
						break;
				}
			}

			// DISPLAY
			for(vector<char>::iterator i = input.begin(); i != input.end(); i++)
				cout << *i;
		}
		else
		{
			// COMPLIMENT (-c)
			for(vector<char>::iterator i = input.begin(); i != input.end(); i++)
			{
				for(string::iterator j = s1.begin(); j != s1.end(); j++)
				{
					string::iterator k = s2.end()-1;
					if(*i != *j && *i != *(j+1) && *i != *(j+2) && *i != *(j+3))
						*i = *k;
					else
						break;
				}
			}

			// DISPLAY
			for(vector<char>::iterator i = input.begin(); i != input.end(); i++)
				cout << *i;
		}
	} 
	else
	{
		if(del == 1)
		{
			// DELETE (-d)
			for(vector<char>::iterator i = input.begin(); i != input.end(); i++)
			{
				for(string::iterator j = s1.begin(); j != s1.end(); j++)
				{
					if(*i == *j)
					{
						input.erase(i);
					}
				}
			}

			// DISPLAY
			for(vector<char>::iterator i = input.begin(); i != input.end(); i++)
			{
				cout << *i;
			}
		}
		else
		{
			for(size_t i = 0; i < input.size(); i++)
			{
				cout << input[i];
			}
		}
	}


	return 0;
}
