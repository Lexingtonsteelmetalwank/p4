#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
#include <iostream>
#include <map>
#include <strings.h>

using std::string;
using std::cin;
using std::cout;
using std::endl;
using std::map;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of sort.  Supported options:\n\n"
"   -r,--reverse        Sort descending.\n"
"   -f,--ignore-case    Ignore case.\n"
"   -u,--unique         Don't output duplicate lines.\n"
"   --help              Show this message and exit.\n";

struct igncaseComp {
	bool operator()(const string& s1, const string s2) {
		return (strcasecmp(s1.c_str(),s2.c_str()) < 0);
	}
};

int main(int argc, char *argv[])
{
	// define long options
	static int descending=0, ignorecase=0, unique=0;
	static struct option long_opts[] = {
		{"reverse",       no_argument,   0, 'r'},
		{"ignore-case",   no_argument,   0, 'f'},
		{"unique",        no_argument,   0, 'u'},
		{"help",          no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "rfuh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'r':
				descending = 1;
				break;
			case 'f':
				ignorecase = 1;
				break;
			case 'u':
				unique = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	if(ignorecase == 0)
	{
		//Creates a map that is case sensitive
		map<string,int> M1;

		//Enters words into the map
		string word;
		while(cin >> word) M1[word]++;

		if(unique == 1)
		{
			for(map<string,int>::iterator i = M1.begin(); i != M1.end(); i++)
			{
				if(i->second > 1)
				{
					M1.erase(i);
				}
			}
		}

		if(descending == 1)
		{
			for(map<string,int>::reverse_iterator ri = M1.rbegin(); ri != M1.rend(); ri++)
			{
				cout << ri->first << endl;
			}
		}
		else
		{
			//Prints the map
			for(map<string,int>::iterator i = M1.begin(); i != M1.end(); i++)
			{
				cout << i->first << ' ';
			}
			cout << endl;
		}
	}
	else
	{
		//Creates a map that is not case sensitive
		map<string,int,igncaseComp> M2;

		//Enters words into the map
		string word;
		while(cin >> word) M2[word]++;

		if(unique == 1)
		{
			for(map<string,int,igncaseComp>::iterator i = M2.begin(); i != M2.end(); i++)
			{
				if(i->second > 1)
				{
					M2.erase(i);
				}
			}
		}

		if(descending == 1)
		{
			for(map<string,int,igncaseComp>::reverse_iterator ri = M2.rbegin(); ri != M2.rend(); ri++)
			{
				cout << ri->first << endl;
			}
		}
		else
		{
			//Prints the map
			for(map<string,int,igncaseComp>::iterator i = M2.begin(); i != M2.end(); i++)
			{
				cout << i->first << ' ';
			}
			cout << endl;
		}
	}

	return 0;
}